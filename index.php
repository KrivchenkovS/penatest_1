<?php
require_once "layout/header.php";
require_once "lib/db_connect.php";
require_once "lib/debug.php";
require_once "functions.php";


$topic = get_topic();


//-----------------pagination------------------------------

isset($_GET['pag']) ? $page = (int)$_GET ['pag'] : $page = (int)1;

$notesOnPage = 1;
$from = ($page - 1) * $notesOnPage;


$query_topic_lim = "SELECT * FROM `topic` WHERE `id` > 0 LIMIT $from,$notesOnPage";
$query = $db_connect->prepare($query_topic_lim);
$query->execute();
$topic_lim = $query->fetchAll(\PDO::FETCH_ASSOC);
//debug($topic_lim);


$query = "SELECT COUNT(*) as count FROM `topic`";
$query = $db_connect->prepare($query);
$query->execute();
$res = $query->fetchAll(\PDO::FETCH_ASSOC);
//debug($res);
$pagesCount = ceil($res[0]['count'] / $notesOnPage);


?>


<!-- content start -->
<main>
    <div class="wrapper__main">
        <h1>Тема опроса</h1>
        <div class="form-block-question">
            <div class="form__header">
                <?php foreach ($topic as $val): ?>
                    <a href="questions.php?page=<?= $val['id'] ?>"><?php echo($val['name']), '</br>'; ?></a>
                <?php endforeach ?>
            </div>
        </div>
    </div>


</main>
<!-- content end -->


<center>
<?php
for ($i = 1; $i < $pagesCount; $i++) {
    if ($page == $i) {
        $class = ' class="active"';
    } else {
        $class = '';
    }
        echo "<div class='wrapper__main_pagination' style='padding: 5px;margin-top: 10px;'><a href='?pag=$i' $class style='padding: 5px;'>$i</a></div>";
}
?>
</center>


<?php
require_once "layout/footer.php";
?>


<?php
/*
 if ($page == $i) {
        echo "<a href='?page=$i' class='active'>$i</a> ";

    } else {

        echo "<a href='?page=$i'>$i</a> ";
    }
 */

/*
1 3
2 6
3 9
4 12


0 3
3 3
6 3
*/
?>


