<?php




try {
    $db_connect = new PDO('mysql:host=localhost;dbname=technical_task', 'root', 'root');
    $db_connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "DB connect - ERROR!: " . $e->getMessage() . "<br/>";
    die();
}


/*
$db_name = 'app/config/db_config.php';
if (file_exists($db_name)) {
    $db_config = require_once $db_name;
}
try {

    $this->db = new \PDO("mysql:host={$db_config['host']};dbname={$db_config['db_name']}", $db_config['user'], $db_config['password']);
} catch (\PDOException $e) {
    die('<b>' . "DB connect error!!!" . '</b>');
}
*/

/*
try {
    $db_connect = new PDO('mysql:host=localhost;dbname=technical_task', 'root', 'root');
    foreach($db_connect->query('SELECT * from `test_topic`') as $row) {
        //print_r($row);

    }
    $db_connect = null;
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
*/
?>