<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


require_once "layout/header.php";
require_once "lib/db_connect.php";
require_once "lib/debug.php";
require_once "functions.php";


$header = get_topic_for_q();

$number = 0;

if (isset($_GET['page'])) {
    $test_id = (int)$_GET['page'];
    $test_data = get_test_data($test_id);
}
debug($test_data);

?>

<!-- content start -->
<main>

    <div class="wrapper__main">
        <form action="" class="form-block-question" method="POST">

            <?php foreach ($header as $item):?>
            <div class="form__topic">
                <h1>Тема: <?php echo $item['name']; ?></h1>
            </div>
            <?php endforeach ?>

            <?php foreach ($test_data as $key=>$item): $i=1; //debug($item);?>
            <div class="form__question">
                <h2>Вопрос № <?php echo $key+1,': ', $item['name']; ?></h2>
            </div>

            <div class="form__questions">


                <?php foreach ($item['answers'] as $key => $answer): ?>

                    <div class="form__question_item">
                        <h3 for="">Ответ №<?php echo $key+1?>: </h3> <label><?php echo $answer; ?></label>
                        <input type="radio" name="<?php echo $answer; ?>" class="btn_radio" value="<?php echo $key?>"><br>
                    </div>

                <?php endforeach ?>


            </div>
            <?php endforeach ?>
            <button class="form__button" type="submit" name="send">Отправить</button>
            <button class="form__button" type="submit" name="send">Результат тестирования</button>
        </form>
    </div>


</main>
<!-- content end -->







<?php
require_once "layout/footer.php";
?>
