-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 29 2021 г., 18:23
-- Версия сервера: 8.0.19
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `technical_task`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int NOT NULL,
  `id_questions` int NOT NULL,
  `answer` varchar(250) NOT NULL,
  `is_correct` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `id_questions`, `answer`, `is_correct`) VALUES
(1, 1, 'бабушка', 1),
(2, 1, 'дедушка', 0),
(3, 1, 'молочник', 0),
(4, 1, 'сосед', 0),
(5, 2, 'брат', 1),
(6, 2, 'друг', 0),
(7, 2, 'сват', 0),
(8, 2, 'одноклассник', 0),
(9, 3, 'пень', 0),
(10, 3, 'дед', 1),
(11, 3, 'пес', 0),
(12, 3, 'молоковоз', 0),
(13, 4, 'полиционер', 0),
(14, 4, 'Вася', 0),
(15, 4, 'батя', 0),
(16, 4, 'дядя', 1),
(17, 5, 'мама', 1),
(18, 5, 'тетя', 0),
(19, 5, 'учительница', 0),
(20, 5, 'домомучительница', 0),
(21, 6, 'береза', 0),
(22, 6, 'ясень', 0),
(23, 6, 'рябина', 0),
(24, 6, 'дуб', 1),
(25, 7, 'береза', 0),
(26, 7, 'каштан', 0),
(27, 7, 'сосна', 1),
(28, 7, 'дуб', 0),
(29, 8, 'рябинка', 0),
(30, 8, 'березка', 1),
(31, 8, 'дубок', 0),
(32, 8, 'осинка', 0),
(33, 9, 'дуб', 0),
(34, 9, 'ива', 0),
(35, 9, 'клен', 0),
(36, 9, 'береза', 1),
(37, 10, 'дубок', 0),
(38, 10, 'рябинка', 0),
(39, 10, 'березка', 0),
(40, 10, 'елочка', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_test_topic` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `name`, `id_test_topic`) VALUES
(1, 'Кто любить не устает,\r\nПироги для нас печет,\r\nВкусные оладушки?\r\nЭто наша...', 1),
(2, 'Я у мамы не один,\r\nУ неё ещё есть сын,\r\nРядом с ним я маловат,\r\nДля меня он — старший..', 1),
(3, 'Он трудился не от скуки,\r\nУ него в мозолях руки,\r\nА теперь он стар и сед —\r\nМой родной, любимый...', 1),
(4, 'Кто же с маминой сестрой\r\nПриезжает к нам порой?\r\nНа меня с улыбкой глядя,\r\nЗдравствуй! — говорит мне...', 1),
(5, 'Кто милее всех на свете?\r\nКого любят очень дети?\r\nНа вопрос отвечу прямо:\r\n— Всех милее наша ...', 1),
(6, 'На каких деревьях растут жёлуди?', 2),
(7, 'У какого дерева вместо листьев растут иголки?', 2),
(8, 'Про какое дерево поется самая известная русская народная песня?', 2),
(9, 'У какого дерева кора белая в чёрную полоску? Её называют берестой.', 2),
(10, 'Про какое дерево поется самая известная новогодняя детская песня?', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `topic`
--

CREATE TABLE `topic` (
  `id` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `topic`
--

INSERT INTO `topic` (`id`, `name`) VALUES
(1, 'Детские загадки'),
(2, 'Викторина для детей \"Деревья\"'),
(3, 'тема 3'),
(4, 'тема 4');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
