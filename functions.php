<?php
require_once "lib/db_connect.php";
require_once "lib/debug.php";

$page = $_GET['page'];


// запрос на получение тем опроса
function get_topic()
{
    global $db_connect;
    $query = "SELECT * FROM `topic`";
    $query = $db_connect->prepare($query);
    $query->execute();
    return $query->fetchAll(\PDO::FETCH_ASSOC);
}

// запрос на получение темы опроса выбранной пользователем на index.php
function get_topic_for_q()
{
    global $db_connect;
    global $page;
    $query = "SELECT * FROM `topic` WHERE `id` = $page";
    $query = $db_connect->prepare($query);
    $query->execute();
    return $query->fetchAll(\PDO::FETCH_ASSOC);
}

// Запрос на получение комплекта воросов по выбранной теме
function get_questions()
{
    global $db_connect;
    global $page;
    $query = "SELECT * FROM `questions` `topic` WHERE `id_test_topic` = $page";
    $query = $db_connect->prepare($query);
    $query->execute();
    return $query->fetchAll(\PDO::FETCH_ASSOC);


}

//получение данных теста
function get_test_data($test_id)
{
    if (!$test_id) return 'Введено не корректное значение.';
    global $db_connect;
    global $page;
    $query = $db_connect->prepare("SELECT * From `questions` WHERE `id_test_topic` = $page"); // Get all questions
    $query->execute();
    $questions = $query->fetchAll(\PDO::FETCH_ASSOC);

    $data = []; // Questions with answers

    foreach ($questions as $question) {
        //debug($question);

        // Перебираем все вопросы (questions) что бы получить все ответы (answers) по идентификатору вороса (by question id)

        $query = $db_connect->prepare("SELECT `answer` FROM `answers` WHERE `id_questions` = :questionId");
        $query->bindParam(':questionId',  $question['id'], PDO::PARAM_INT);
        $query->execute();
        $answers = $query->fetchAll(\PDO::FETCH_COLUMN);

        /*
//       Запрос на получение верного и неверного ответа для выбранных вопросов

        $query = $db_connect->prepare("SELECT `is_correct` FROM `answers` WHERE `id_questions` = :questionId");
        $query->bindParam(':questionId',  $question['id'], PDO::PARAM_INT);
        $query->execute();
        $correct = $query->fetchAll(\PDO::FETCH_COLUMN);

        */


        // делаем слияние answers с question
        $question['answers'] = $answers;
        $data[] = $question;

    }

    return $data;
//Сложный запрос на получения данных по сравнению полей таблице
/*
$query = "SELECT q.`name`, q.`id_test_topic`, a.`id`, a.`id_questions`, a.`answer` FROM `questions` q LEFT JOIN `answers` a ON q.`id`=a.`id_questions` WHERE q.`id_test_topic` = $test_id";
*/
}



